package it.univr.flickrclient;

import it.univr.flickrclient.controller.Controller;
import it.univr.flickrclient.model.Model;

public class MVC {

    public final Model model;
    public final Controller controller;

    public MVC(Model model, Controller controller) {
        this.model = model;
        this.controller = controller;
    }
}