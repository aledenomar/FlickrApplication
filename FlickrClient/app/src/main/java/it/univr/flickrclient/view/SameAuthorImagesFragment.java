package it.univr.flickrclient.view;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.HashMap;

import it.univr.flickrclient.FlickrClientApplication;
import it.univr.flickrclient.MVC;
import it.univr.flickrclient.R;
import it.univr.flickrclient.controller.DownloadImageAsyncTask;
import it.univr.flickrclient.controller.SearchImagesAsyncTask;

import static it.univr.flickrclient.controller.Controller.BUNDLE_INFOS;

    /**
    * Fragment che mostra la lista delle immagini relative ad uno stesso autore
    */

public class SameAuthorImagesFragment extends Fragment {

    private ListView listview;
    private View view;
    private String image_url;
    private MVC mvc;

    public SameAuthorImagesFragment(){}

    //----------------------------------------------------------------------------------------------
    // Metodo di creazione della view del fragment
    //----------------------------------------------------------------------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mvc = ((FlickrClientApplication) getActivity().getApplication()).getMVC();

        // Se la view non è inizializzata la creo da zero.
        if(view == null) {
            view = inflater.inflate(R.layout.listviewimages_fragment, container, false);

            listview = (ListView) view.findViewById(R.id.list_view_results);

            // Listener per il click breve, porta al fragment che mostra la singola immagine
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    ResultsAdapter ra = (ResultsAdapter) listview.getAdapter();

                    image_url = ra.getItem(i).getUrl();

                    HashMap<String, String> map = new HashMap<>();
                    map.put("id", ra.getItem(i).getId());
                    map.put("title", ra.getItem(i).getTitle());
                    map.put("url", image_url);

                    SelectedImageFragment frag = new SelectedImageFragment();
                    Bundle args = new Bundle();
                    args.putSerializable(BUNDLE_INFOS, map);
                    frag.setArguments(args);

                    mvc.controller.launchFragment(getActivity(), frag, true);

                }
            });

            registerForContextMenu(listview);

            mvc.controller.launchTask(new SearchImagesAsyncTask(getActivity(), listview, getArguments().getString("author")));
        }

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Autore: " + getArguments().getString("authorname"));

        return view;
    }


    //----------------------------------------------------------------------------------------------
    // Metodo che viene lanciato quando viene cliccato (click prolungato) l'elemento item della
    // listview e setta gli elementi del menu.
    // A differenza dei menu nelle altre listview, qui non é presente la voce "Stesso autore"
    // perché le immagini sono giá di uno stesso autore
    //----------------------------------------------------------------------------------------------

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderIcon(R.drawable.flickr);
        menu.setHeaderTitle("Select an option");
        getActivity().getMenuInflater().inflate(R.menu.menu_context, menu);

        // Setto il bottone Same Author non visibile in modo da non vederlo nel menu contestuale al click lungo
        menu.getItem(1).setVisible(false);
    }

    //----------------------------------------------------------------------------------------------
    // Metodo che viene lanciato quando viene cliccato (click prolungato) l'elemento item
    // della listview e setta gli elementi del menu.
    // Se si preme sull'elemento del menu si lancia il task per la condivisione dell'immagine.
    //----------------------------------------------------------------------------------------------

    @UiThread
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        ResultsAdapter ra = (ResultsAdapter) listview.getAdapter();
        image_url = ra.getItem(info.position).getUrl();

        switch (item.getItemId()) {
            case R.id.context_menu_share:

                mvc.controller.launchTask(new DownloadImageAsyncTask(getActivity(), this, image_url));

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}