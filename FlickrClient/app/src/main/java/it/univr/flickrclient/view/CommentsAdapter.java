package it.univr.flickrclient.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import it.univr.flickrclient.R;
import it.univr.flickrclient.model.Model;

    /**
    * Classe contenente la ridefinizione dell'adapter che viene utilizzato per i commenti
    * delle immagini.
    */
public class CommentsAdapter extends ArrayAdapter<Model.Message> {

    private Context context;
    private ArrayList<Model.Message> data;

    //----------------------------------------------------------------------------------------------
    // Costruttore dell'adapter dei commenti delle immagini
    //----------------------------------------------------------------------------------------------

    public CommentsAdapter(Context context, ArrayList<Model.Message> data) {
        super(context, R.layout.message_layout, data);
        this.context = context;     // Contesto dell'activity che crea l'adapter
        this.data = data;           // Lista di elementi necessari per la creazione della listview
    }


    //----------------------------------------------------------------------------------------------
    // Metodo che ritorna l'elemento in posizione position della lista
    //----------------------------------------------------------------------------------------------

    @Override
    public Model.Message getItem(int position) {
        return data.get(position);
    }


    //----------------------------------------------------------------------------------------------
    // Metodo di creazione della view. Viene popolata la listview con le informazioni
    // relative ai commenti scaricati dell'immagine selezionata.
    //----------------------------------------------------------------------------------------------

    @NonNull
    @Override
    public View getView(int i, View view, @NonNull ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.message_layout, viewGroup, false);
        Spanned result;

        // parametri del messaggio
        TextView name = (TextView) rowView.findViewById(R.id.message_sender);
        TextView date = (TextView) rowView.findViewById(R.id.message_time);
        TextView body = (TextView) rowView.findViewById(R.id.message_body);

        //compilazione
        name.setText(data.get(i).getAuthor());
        date.setText(formatDateTime(data.get(i).getDate()));

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(data.get(i).getBody(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(data.get(i).getBody());
        }

        // compilazione e formattazione
        body.setText(result);
        body.setMovementMethod(LinkMovementMethod.getInstance());

        return rowView;
    }


    //----------------------------------------------------------------------------------------------
    // Formatta la data del commento in modo da avere la formattazione "dd/MM/yyyy HH:mm"
    //----------------------------------------------------------------------------------------------

    private String formatDateTime(String date) {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date(Long.parseLong(date) * 1000));
    }
}
