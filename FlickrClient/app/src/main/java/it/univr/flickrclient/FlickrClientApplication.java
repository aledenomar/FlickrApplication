package it.univr.flickrclient;

import android.app.Application;

import it.univr.flickrclient.controller.Controller;
import it.univr.flickrclient.model.Model;

public class FlickrClientApplication extends Application {

    private MVC mvc;

    @Override
    public void onCreate() {
        super.onCreate();
        mvc = new MVC(new Model(), new Controller());
    }

    public MVC getMVC() {
        return mvc;
    }
}
