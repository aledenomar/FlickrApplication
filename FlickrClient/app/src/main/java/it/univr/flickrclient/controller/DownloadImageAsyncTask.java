package it.univr.flickrclient.controller;

import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.UiThread;
import android.support.annotation.WorkerThread;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import it.univr.flickrclient.FlickrClientApplication;
import it.univr.flickrclient.MVC;

    //----------------------------------------------------------------------------------------------
    // Task per l'utilizzo del comando Share.
    // Ricava e salva il bitmap dell'immagine selezionata e successivamente lancia l'intent
    // per la condivisione.
    //----------------------------------------------------------------------------------------------

public class DownloadImageAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private Context context;
    private String image_url;
    private Bitmap bitmap;

    private Dialog dialog;
    private Fragment fragment;

    private MVC mvc;

    //----------------------------------------------------------------------------------------------
    // Costruttore del Task Asincrono.
    //----------------------------------------------------------------------------------------------

    public DownloadImageAsyncTask(Context context, Fragment fragment, String url){
        this.context = context;
        this.image_url = url;
        this.fragment = fragment;

        mvc = ((FlickrClientApplication) context.getApplicationContext()).getMVC();
    }

    //----------------------------------------------------------------------------------------------
    // Metodo eseguito dall'AsyncTask prima del lancio della thread in background
    //----------------------------------------------------------------------------------------------

    @UiThread
    @Override
    protected void onPreExecute() {

        dialog = ProgressDialog.show(context, "",
                "Caricamento...", true);
        dialog.setCancelable(false);

    }

    //----------------------------------------------------------------------------------------------
    // Metodo che esegue il task in background ed effettua il download
    // dell'immagine dell'URL passato.
    // L'immagine viene salvata in una variabile bitmap.
    //----------------------------------------------------------------------------------------------

    @Override
    @WorkerThread
    protected Boolean doInBackground(Void... voids) {

        try {
            java.net.URL url = new java.net.URL(image_url);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            bitmap = BitmapFactory.decodeStream(input);

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return null;
    }

    //----------------------------------------------------------------------------------------------
    // Metodo lanciato successivamente al doInBackground
    //----------------------------------------------------------------------------------------------

    @UiThread
    @Override
    protected void onPostExecute(final Boolean success) {
        dialog.dismiss();
        mvc.controller.launchIntent(fragment, mvc.controller.shareImage(context, bitmap));
    }
}