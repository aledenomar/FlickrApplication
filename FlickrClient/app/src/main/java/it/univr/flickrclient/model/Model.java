package it.univr.flickrclient.model;

public class Model {

    public static class Images {

        public String id;
        public String title;
        public String url;
        public String description;
        private String author;
        private String authorName;

        public Images(String id, String title, String url, String description, String author, String authorName) {
            this.id = id;
            this.title = title;
            this.url = url;
            this.description = description;
            this.author = author;
            this.authorName = authorName;
        }

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getUrl() {
            return url;
        }

        public String getDescription() {
            return description;
        }

        public String getAuthor() {
            return author;
        }

        public String getAuthorName() { return authorName; }
    }

    public static class Message{

        private String author;
        private String date;
        private String body;

        public Message(String author, String date, String body){
            this.author = author;
            this.date = date;
            this.body = body;
        }

        public String getAuthor() {
            return author;
        }

        public String getDate() {
            return date;
        }

        public String getBody() {
            return body;
        }
    }

}
