package it.univr.flickrclient.view;

import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import it.univr.flickrclient.FlickrClientApplication;
import it.univr.flickrclient.MVC;
import it.univr.flickrclient.R;

import static it.univr.flickrclient.controller.Controller.OPTION_LASTIMAGES;
import static it.univr.flickrclient.controller.Controller.OPTION_POPULARIMAGES;


    /**
    * Fragment che compare soltanto nella versione per tablet.
    * É il menu laterale che nella versione per smartphone é rappresentato all'interno del drawer
    */

public class TabletMenuFragment extends Fragment implements NavigationView.OnNavigationItemSelectedListener{

    private MVC mvc;

    public TabletMenuFragment() {}

    //----------------------------------------------------------------------------------------------
    // Metodo di creazione della view del fragment
    //----------------------------------------------------------------------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tablet_menu_fragment_layout, container, false);

        mvc = ((FlickrClientApplication) getActivity().getApplication()).getMVC();

        NavigationView navigationView = (NavigationView) view.findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);

        MenuItem menuItemSelected = navigationView.getMenu().findItem(R.id.drawerbutton_searchimages);
        menuItemSelected.setChecked(true);
        
        navigationView.setNavigationItemSelectedListener(this);

        return view;
    }

    //----------------------------------------------------------------------------------------------
    // Metodo che gestisce l'evento della selezione di un oggetto nel menu laterale.
    // Le possibili scelte sono: SEARCH, LAST IMAGES, POPULAR IMAGES e APP INFORMATION
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.drawerbutton_searchimages) {

            mvc.controller.launchFragment(getActivity(), new SearchImagesFragment(), false);

        } else if (id == R.id.drawerbutton_lastimages) {

            ImageListFragment frag = new ImageListFragment();

            //Creazione del Bundle args per passare gli elementi al fragment
            Bundle args = new Bundle();

            args.putString(OPTION_LASTIMAGES, OPTION_LASTIMAGES);
            frag.setArguments(args);
            mvc.controller.launchFragment(getActivity(), frag, false);

        } else if (id == R.id.drawerbutton_popularimages) {

            ImageListFragment frag = new ImageListFragment();
            Bundle args = new Bundle();
            args.putString(OPTION_POPULARIMAGES, OPTION_POPULARIMAGES);
            frag.setArguments(args);
            mvc.controller.launchFragment(getActivity(), frag, false);

        } else if (id == R.id.drawerbutton_appinformations) {

            mvc.controller.infoDialog(getActivity());

        }
        return true;
    }
}