package it.univr.flickrclient.controller;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.UiThread;
import android.support.v4.content.FileProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import it.univr.flickrclient.R;
import it.univr.flickrclient.view.AppInformation;

    /**
     * Classe Controller che gestisce le funzionalità dell'applicazione.
     * Controller della classe MVC
     */

public class Controller {

    // Costanti di decizione sulle azioni da intraprendere.
    public final static String OPTION_SEARCHIMAGES = "SEARCH";
    public final static String OPTION_LASTIMAGES = "LAST";
    public final static String OPTION_POPULARIMAGES = "POPULAR";

    // Nome della mappa che contiene le informazioni scaricate da Flickr
    public final static String BUNDLE_INFOS = "BUNDLE_INFORMATION";

    // Costanti contenenti i valori dei tag di richiesta a Flickr
    public final static String FLICKR_IMAGE_ID = "id";
    public final static String FLICKR_IMAGE_TITLE = "title";
    public final static String FLICKR_IMAGE_URLZ = "url_z";
    public final static String FLICKR_IMAGE_URLL = "url_l";
    public final static String FLICKR_IMAGE_URLSQ = "url_sq";
    public final static String FLICKR_IMAGE_DESCRIPTION = "description";
    public final static String FLICKR_IMAGE_CONTENT = "_content";
    public final static String FLICKR_AUTHOR_ID = "owner";
    public final static String FLICKR_AUTHOR_NAME = "ownername";

    public final static String FLICKR_MESSAGE_AUTHOR = "authorname";
    public final static String FLICKR_MESSAGE_DATE = "datecreate";
    public final static String FLICKR_MESSAGE_TEXT = "_content";



    //----------------------------------------------------------------------------------------------
    // Costante contenente la chiave di Flickr.
    // Viene utilizzata in tutte le richieste al sito.
    //----------------------------------------------------------------------------------------------

    public final static String FLICKR_KEY = "b851a00bc135c0baf0474bbfc896c52b";


    //----------------------------------------------------------------------------------------------
    // Metodo che ritorna l'URI in cui viene salvata l'immagine
    // Viene utilizzata successivamente al download dell'immagine in locale.
    // L'immagine viene scaricata nella Cache.
    //----------------------------------------------------------------------------------------------

    @UiThread
    public Uri shareImage(Context context, Bitmap bitmap) {
        try {
            File cachePath = new File(context.getCacheDir(), "images");
            cachePath.mkdirs();
            FileOutputStream stream = new FileOutputStream(cachePath + "/image.png");
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        File imagePath = new File(context.getCacheDir(), "images");
        File newFile = new File(imagePath, "image.png");
        return FileProvider.getUriForFile(context, "it.univr.flickrclient.fileprovider", newFile);
    }

    //----------------------------------------------------------------------------------------------
    //  Metodo per il lancio dei Fragment nell'app. Serve per cambiare dal fragment attivo
    //  a quello nuovo passato come parametro alla funzione.
    //----------------------------------------------------------------------------------------------

    @UiThread
    public void launchFragment(Activity activity, Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction().replace(R.id.content_fragment, fragment);
        if(addToBackStack){
            transaction.addToBackStack(null).commit();
        } else {

            // Rimozione dei Fragment dal BackStack. Serve per non ricaricare Fragment non aggiornati
            // alla pressione del tasto back.
            for(int i = 0; i < activity.getFragmentManager().getBackStackEntryCount(); ++i) {
                activity.getFragmentManager().popBackStack();
            }
            transaction.commit();
       }
    }

    //----------------------------------------------------------------------------------------------
    // Lancia il dialogo delle informazioni.
    //----------------------------------------------------------------------------------------------

    @UiThread
    public void infoDialog(Activity activity){
        new AppInformation(activity).alert.show();
    }

    //----------------------------------------------------------------------------------------------
    // Metodo necessario per l'esecuzione del task passato come argomento
    //----------------------------------------------------------------------------------------------

    @UiThread
    public void launchTask(AsyncTask<Void, Void, Boolean> task){
        task.execute();
    }

    //----------------------------------------------------------------------------------------------
    // Metodo che si occupa del lancio dell'intent per la condivisione delle immagini
    // con le altre app.
    //----------------------------------------------------------------------------------------------

    @UiThread
    public void launchIntent(Fragment fragment, Uri contentUri){
        if (contentUri != null) {
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.setDataAndType(contentUri, fragment.getActivity().getContentResolver().getType(contentUri));
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
            fragment.startActivity(Intent.createChooser(shareIntent, "Choose an app"));
        }
    }
}