package it.univr.flickrclient.view;

import android.app.Fragment;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.bumptech.glide.Glide;

import java.util.HashMap;

import it.univr.flickrclient.FlickrClientApplication;
import it.univr.flickrclient.MVC;
import it.univr.flickrclient.R;
import it.univr.flickrclient.controller.DownloadImageInformationAsyncTask;

import static it.univr.flickrclient.controller.Controller.BUNDLE_INFOS;

    /**
    * Fragment che mostra l'immagine e i commenti ad essa relativi.
    * Viene lanciato quando viene premuto un item sulla lista delle immagini.
    */

public class SelectedImageFragment extends Fragment {

    private ImageView image;
    private String image_id;
    private String image_url;
    private String image_title;
    private View view;
    private MVC mvc;

    public SelectedImageFragment() {}

    //----------------------------------------------------------------------------------------------
    // Metodo di creazione della view del fragment
    //----------------------------------------------------------------------------------------------

    @Override
    @UiThread
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Salva la MVC condivisa nella variabile
        mvc = ((FlickrClientApplication) getActivity().getApplication()).getMVC();

        // Se la vista è nulla allora deve essere creata, altrimenti si riusa la view creata in precedenza
        if (view == null) {
            view = inflater.inflate(R.layout.selected_image_fragment, container, false);

            Bundle args = getArguments();

            if (args != null) {

                HashMap<String, String> map = (HashMap<String, String>) args.getSerializable(BUNDLE_INFOS);

                image_id = map.get("id");
                image_url = map.get("url");
                image_title = map.get("title");

            }

            final ListView listview = (ListView) view.findViewById(R.id.selected_image_comments);
            image = (ImageView) view.findViewById(R.id.selected_image_src);
            Glide.with(getActivity()).load(image_url).into(image);

            mvc.controller.launchTask(new DownloadImageInformationAsyncTask(getActivity(), image_id, listview));
        }

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(image_title);

        return view;
    }

    //----------------------------------------------------------------------------------------------
    // Metodo per la creazione del fragment.
    // Necessario per notificare al fragment la presenza di un menu contestuale.
    //----------------------------------------------------------------------------------------------

    @Override
    @UiThread
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    //----------------------------------------------------------------------------------------------
    // Metodo per la crezione del menu nella toolbar. É presente nel menu solo il tasto "condividi"
    //----------------------------------------------------------------------------------------------

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_share, menu);
    }


    //----------------------------------------------------------------------------------------------
    // Metodo per la configurazione del menu contestuale. Premendo il tasto "condividi" si
    // lancia l'intent relativo alla condivisione dell'immagine
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_share:
                mvc.controller.launchIntent(this, mvc.controller.shareImage(getActivity().getBaseContext(), ((BitmapDrawable) image.getDrawable()).getBitmap()));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}