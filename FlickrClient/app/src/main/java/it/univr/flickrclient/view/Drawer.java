package it.univr.flickrclient.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import it.univr.flickrclient.FlickrClientApplication;
import it.univr.flickrclient.MVC;
import it.univr.flickrclient.R;

import static it.univr.flickrclient.controller.Controller.OPTION_LASTIMAGES;
import static it.univr.flickrclient.controller.Controller.OPTION_POPULARIMAGES;

    /**
    * Classe Drawer creata soltanto nella versione per smartphone dell'applicazione.
    * Implementa OnNavigationItemSelectedListener per la creazione del menu laterale.
    */

public class Drawer extends DrawerLayout implements NavigationView.OnNavigationItemSelectedListener {

    private final Activity activity;
    private DrawerLayout drawer;
    private MVC mvc;


    //----------------------------------------------------------------------------------------------
    // Costruttore della classe Drawer. Viene preso l'oggetto mvc dal controllore
    // e viene settata la creazione effettiva del drawer
    //----------------------------------------------------------------------------------------------

    public Drawer(Activity activity, Toolbar toolbar) {
        super(activity);

        this.activity = activity;
        mvc = ((FlickrClientApplication) activity.getApplication()).getMVC();

        drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(activity, drawer, toolbar, R.string.draweropened, R.string.drawerclosed) {

            /**
             * Metodo che si occupa della chiusura del drawer
             */

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            /**
             * Metodo che si occupa dell'apertura del drawer
             */

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                CloseKeyboardByDrawer();
            }

            /**
             * Metodo che si occupa dello slide, ovvero movimento laterale, del drawer.
             */

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                CloseKeyboardByDrawer();
            }
        };

        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        NavigationView navigationView = (NavigationView) drawer.findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);

        MenuItem menuSelectedItem = navigationView.getMenu().findItem(R.id.drawerbutton_searchimages);
        menuSelectedItem.setChecked(true);

        navigationView.setNavigationItemSelectedListener(this);

    }


    //----------------------------------------------------------------------------------------------
    // Metodo senza argomenti passati in input che si occupa di chiudere la tastiera
    // virtuale all'apertura del drawer
    //----------------------------------------------------------------------------------------------

    public void CloseKeyboardByDrawer(){
        InputMethodManager inputMethodManager = (InputMethodManager)
                activity.getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }


    //----------------------------------------------------------------------------------------------
    // Metodo che gestisce l'evento della selezione di un oggetto nel menu laterale.
    // Una volta selezionato l'elemento il Drawer viene chiuso.
    // Le possibili scelte sono: SEARCH, LAST IMAGES, POPULAR IMAGES e APP INFORMATION
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        ImageListFragment fragment = new ImageListFragment();
        Bundle args = new Bundle();

        switch (item.getItemId()){
            case R.id.drawerbutton_searchimages:
                mvc.controller.launchFragment(activity, new SearchImagesFragment(), false);
                break;
            case R.id.drawerbutton_lastimages:
                args.putString(OPTION_LASTIMAGES, OPTION_LASTIMAGES);
                fragment.setArguments(args);
                mvc.controller.launchFragment(activity, fragment, false);
                break;
            case R.id.drawerbutton_popularimages:
                args.putString(OPTION_POPULARIMAGES, OPTION_POPULARIMAGES);
                fragment.setArguments(args);
                mvc.controller.launchFragment(activity, fragment, false);
                break;
            case R.id.drawerbutton_appinformations:
                mvc.controller.infoDialog(activity);
        }

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    public DrawerLayout getDrawer() {
        return drawer;
    }
}