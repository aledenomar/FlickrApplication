package it.univr.flickrclient.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import it.univr.flickrclient.R;

public class ContentFragment extends Fragment {

    private View view;

    public ContentFragment(){}

    //----------------------------------------------------------------------------------------------
    // Metodo di creazione della view del fragment
    //----------------------------------------------------------------------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.content_fragment_layout, container, false);

        return view;
    }
}