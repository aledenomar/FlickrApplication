package it.univr.flickrclient.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.Html;

import it.univr.flickrclient.R;

    /**
     * Classe che estende AlertDialog per la creazione del dialog di informazioni
     */

public class AppInformation extends AlertDialog {

    public final AlertDialog alert;

    //----------------------------------------------------------------------------------------------
    // Costruttore di AppInformation. Crea l'AlertDialog contenente autore,
    // versione la data di creazione.
    // activity Activity in cui viene creato l'AppInformation.
    //----------------------------------------------------------------------------------------------

    public AppInformation(Activity activity) {
        super(activity);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        // Messaggio formattato in HTML per avere maggior controllo sulla formattazione del testo.
        builder.setMessage(Html.fromHtml("<b>Autori:</b><br><i>Alessandro Saletti - VR404101<br>Denis Quartaroli - VR399554<br>Omar Dabbagh - VR404852</i><br><br><b>Versione:</b> 1.4<br><b>Data:</b> 15 Febbraio 2018"));
        builder.setTitle(activity.getResources().getString(R.string.app_name));
        builder.setCancelable(false);

        // Inserisco nel Dialog l'icona dell'applicazione.
        builder.setIcon(R.drawable.flickr);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        alert = builder.create();
    }
}