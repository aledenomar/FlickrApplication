package it.univr.flickrclient.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.annotation.WorkerThread;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import it.univr.flickrclient.FlickrClientApplication;
import it.univr.flickrclient.MVC;
import it.univr.flickrclient.model.Model;
import it.univr.flickrclient.view.ResultsAdapter;

import static it.univr.flickrclient.controller.Controller.FLICKR_AUTHOR_ID;
import static it.univr.flickrclient.controller.Controller.FLICKR_AUTHOR_NAME;
import static it.univr.flickrclient.controller.Controller.FLICKR_IMAGE_CONTENT;
import static it.univr.flickrclient.controller.Controller.FLICKR_IMAGE_DESCRIPTION;
import static it.univr.flickrclient.controller.Controller.FLICKR_IMAGE_ID;
import static it.univr.flickrclient.controller.Controller.FLICKR_IMAGE_TITLE;
import static it.univr.flickrclient.controller.Controller.FLICKR_IMAGE_URLL;
import static it.univr.flickrclient.controller.Controller.FLICKR_IMAGE_URLSQ;
import static it.univr.flickrclient.controller.Controller.FLICKR_IMAGE_URLZ;
import static it.univr.flickrclient.controller.Controller.OPTION_LASTIMAGES;
import static it.univr.flickrclient.controller.Controller.OPTION_POPULARIMAGES;
import static it.univr.flickrclient.controller.Controller.OPTION_SEARCHIMAGES;

    //----------------------------------------------------------------------------------------------
    // Task generale per la ricerca delle immagini
    //----------------------------------------------------------------------------------------------

public class SearchImagesAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private final Activity activity;

    private JSONObject jsonObject;

    private AlertDialog dialog;
    private String query;

    private MVC mvc;

    private ListView listView;

    private ArrayList<Model.Images> images;

    private String searchImagesURL;
    private String lastImagesURL;
    private String popularImagesURL;
    private String sameAuthorURL;

    private String choose;


    //----------------------------------------------------------------------------------------------
    // Costruttore del task asincrono. Ricerca con parola chiave personalizzata.
    // Lancia UnsupportedEncodingException se non riesce a convertire la stringa in UTF-8
    //----------------------------------------------------------------------------------------------

    public SearchImagesAsyncTask(Activity act, String searchString, ListView listView) {

        this.activity = act;                     //Activity di riferimento
        this.listView = listView;
        images = new ArrayList<>();

        mvc = ((FlickrClientApplication) act.getApplication()).getMVC();

        searchImagesURL = String.format("https://api.flickr.com/services/rest?method=flickr.photos.search&api_key=%s&text=%s&extras=url_z,description,tags&per_page=50&format=json&nojsoncallback=1",
                mvc.controller.FLICKR_KEY,
                searchString.trim().replace(" ", "%20"));

        choose = OPTION_SEARCHIMAGES;

    }

    //----------------------------------------------------------------------------------------------
    // Costruttore del task asincrono per la ricerca di immagini popolari, ultime, stesso autore
    //----------------------------------------------------------------------------------------------

    public SearchImagesAsyncTask(Activity act, ListView listView, String choose) {

        this.activity = act;                //Activity di riferimento
        this.listView = listView;           //Listview da popolare
        this.choose = choose;               //Stringa che identifica la scelta
        images = new ArrayList<>();

        mvc = ((FlickrClientApplication) act.getApplication()).getMVC();

        switch (choose){
            case OPTION_LASTIMAGES:
                lastImagesURL = String.format("https://api.flickr.com/services/rest?method=flickr.photos.getRecent&api_key=%s&extras=owner_name,url_sq,url_l,description,tags&per_page=50&format=json&nojsoncallback=1",
                        mvc.controller.FLICKR_KEY);
                break;
            case OPTION_POPULARIMAGES:
                popularImagesURL = String.format("https://api.flickr.com/services/rest?method=flickr.interestingness.getList&api_key=%s&extras=owner_name,url_sq,url_l,description,tags&per_page=50&format=json&nojsoncallback=1",
                        mvc.controller.FLICKR_KEY);
                break;
            default:
                sameAuthorURL = String.format("https://api.flickr.com/services/rest?method=flickr.photos.search&api_key=%s&user_id=%s&extras=owner_name,url_sq,url_l,description,tags&per_page=50&format=json&nojsoncallback=1",
                        mvc.controller.FLICKR_KEY,
                        choose);

                Log.d("SAMEAUTHOR", sameAuthorURL);
        }


    }

    //----------------------------------------------------------------------------------------------
    // Metodo eseguito dall'AsyncTask prima del lancio della thread in background
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onPreExecute() {
        dialog = ProgressDialog.show(activity, "",
                "Caricamento...", true);
        dialog.setCancelable(false);
    }


    //----------------------------------------------------------------------------------------------
    // Thread in background creata dalla thread principale.
    // Scarica le informazioni relative alla lista di immagini da scaricare
    //----------------------------------------------------------------------------------------------

    @Override
    @WorkerThread
    protected Boolean doInBackground(Void... voids) {

        switch (choose) {
            case OPTION_SEARCHIMAGES:
                query = searchImagesURL;
                break;
            case OPTION_LASTIMAGES:
                query = lastImagesURL;
                break;
            case OPTION_POPULARIMAGES:
                query = popularImagesURL;
                break;
            default:
                query = sameAuthorURL;
        }

        Log.d("QUERY", query);

        try {

            URLConnection urlConnection = new URL(query).openConnection();
            InputStream in = urlConnection.getInputStream();
            jsonObject = new JSONObject(IOUtils.toString(in, "UTF-8"));

            JSONArray array = jsonObject.getJSONObject("photos").getJSONArray("photo");

            // In base alla query il risultato delle api varia (i parametri esistenti), per questo esiste lo switch
            switch (choose) {
                case OPTION_SEARCHIMAGES:
                    for (int i = 0; i < array.length(); i++) {
                        if (array.getJSONObject(i).has(FLICKR_IMAGE_URLZ)) {
                            images.add(new Model.Images(
                                    array.getJSONObject(i).getString(FLICKR_IMAGE_ID),
                                    array.getJSONObject(i).getString(FLICKR_IMAGE_TITLE).isEmpty() ? "No title" : array.getJSONObject(i).getString(FLICKR_IMAGE_TITLE),
                                    array.getJSONObject(i).getString(FLICKR_IMAGE_URLZ),
                                    array.getJSONObject(i).getJSONObject(FLICKR_IMAGE_DESCRIPTION).getString(FLICKR_IMAGE_CONTENT).isEmpty() ? "No description" : array.getJSONObject(i).getJSONObject(FLICKR_IMAGE_DESCRIPTION).getString(FLICKR_IMAGE_CONTENT),
                                    array.getJSONObject(i).getString(FLICKR_AUTHOR_ID),
                                    (array.getJSONObject(i).has(FLICKR_AUTHOR_NAME)?array.getJSONObject(i).getString(FLICKR_AUTHOR_NAME):array.getJSONObject(i).getString(FLICKR_AUTHOR_ID))));
                        }
                    }
                    break;
                default:
                    for (int i = 0; i < array.length(); i++) {
                        if (array.getJSONObject(i).has(FLICKR_IMAGE_URLL)) {
                            images.add(new Model.Images(
                                    array.getJSONObject(i).getString(FLICKR_IMAGE_ID),
                                    array.getJSONObject(i).getString(FLICKR_IMAGE_TITLE).isEmpty() ? "No title" : array.getJSONObject(i).getString(FLICKR_IMAGE_TITLE),
                                    array.getJSONObject(i).getString(FLICKR_IMAGE_URLL),
                                    array.getJSONObject(i).getJSONObject(FLICKR_IMAGE_DESCRIPTION).getString(FLICKR_IMAGE_CONTENT).isEmpty() ? "No description" : array.getJSONObject(i).getJSONObject(FLICKR_IMAGE_DESCRIPTION).getString(FLICKR_IMAGE_CONTENT),
                                    array.getJSONObject(i).getString(FLICKR_AUTHOR_ID),
                                    (array.getJSONObject(i).has(FLICKR_AUTHOR_NAME)?array.getJSONObject(i).getString(FLICKR_AUTHOR_NAME):array.getJSONObject(i).getString(FLICKR_AUTHOR_ID))));
                        } else {
                            images.add(new Model.Images(
                                    array.getJSONObject(i).getString(FLICKR_IMAGE_ID),
                                    array.getJSONObject(i).getString(FLICKR_IMAGE_TITLE).isEmpty() ? "No title" : array.getJSONObject(i).getString(FLICKR_IMAGE_TITLE),
                                    array.getJSONObject(i).getString(FLICKR_IMAGE_URLSQ),
                                    array.getJSONObject(i).getJSONObject(FLICKR_IMAGE_DESCRIPTION).getString(FLICKR_IMAGE_CONTENT).isEmpty() ? "No description" : array.getJSONObject(i).getJSONObject(FLICKR_IMAGE_DESCRIPTION).getString(FLICKR_IMAGE_CONTENT),
                                    array.getJSONObject(i).getString(FLICKR_AUTHOR_ID),
                                    (array.getJSONObject(i).has(FLICKR_AUTHOR_NAME)?array.getJSONObject(i).getString(FLICKR_AUTHOR_NAME):array.getJSONObject(i).getString(FLICKR_AUTHOR_ID))));
                        }
                    }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    //----------------------------------------------------------------------------------------------
    // Metodo lanciato dalla thread principale quando termina
    // l'esecuzione della thread in background.
    // Setta l'adapter alla listview con gli elementi scaricati dal thread in background,
    // crea il Toast per informare che il caricamento é stato
    // completato ed elimina il widget di caricamento
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onPostExecute(final Boolean success) {

        listView.setAdapter(new ResultsAdapter(activity, images));

        if (!(choose.equals(OPTION_SEARCHIMAGES) || choose.equals(OPTION_LASTIMAGES) || choose.equals(OPTION_POPULARIMAGES))) {
            ((AppCompatActivity) activity).getSupportActionBar().setTitle("Autore: " + images.get(0).getAuthorName());
        }

        dialog.dismiss();

    }

}