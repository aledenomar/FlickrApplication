package it.univr.flickrclient.view;

import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import it.univr.flickrclient.FlickrClientApplication;
import it.univr.flickrclient.R;

    /**
    * Activity principale e unica dell'applicazione. Estende AppCompatActivity
    */

public class MainActivity extends AppCompatActivity {

    private Drawer drawer;

    //----------------------------------------------------------------------------------------------
    // Metodo di creazione della view dell'activity
    //----------------------------------------------------------------------------------------------

    @UiThread
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // creazione toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Il drawer è inizializzato solo negli smartphone
        // se è un tablet non crea il drawer(parte laterale)
        if (!getResources().getBoolean(R.bool.isTablet)) {
            drawer = new Drawer(this, toolbar);
        }

        // Lancia il primo fragment (SearchImages) all'avvio dell'app
        ((FlickrClientApplication) getApplication()).getMVC().controller.launchFragment(this, new SearchImagesFragment(), false);

    }

    //----------------------------------------------------------------------------------------------
    // Metodo necessario per la gestione del tasto "back".
    // Viene gestito lo stack della creazione dei fragment: se ci sono fragment nello stack
    // si va ad eliminare il fragment dallo stack in modo da tornare allo stato precedente
    // Se, nel caso del telefono, è aperto il Drawer, lo chiude in modo da tornare alla
    // visualizzazione del fragment attuale.
    //----------------------------------------------------------------------------------------------

    @UiThread
    @Override
    public void onBackPressed() {

        // Se si sta utilizzando un tablet
        if (getResources().getBoolean(R.bool.isTablet)) {
            if (drawer != null && drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getSupportFragmentManager().popBackStack();
                super.onBackPressed();
            }

            // Se si sta utilizzando un telefono
        } else {

            // Ricevo il DrawerLayout dalla classe Drawer per controllare il suo stato di apertura.
            DrawerLayout layout = drawer.getDrawer();

            if (layout != null && layout.isDrawerOpen(GravityCompat.START)) {
                layout.closeDrawer(GravityCompat.START);
            } else {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getSupportFragmentManager().popBackStack();
                super.onBackPressed();
            }
        }
    }
}