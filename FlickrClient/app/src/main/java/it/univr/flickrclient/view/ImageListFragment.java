package it.univr.flickrclient.view;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.HashMap;

import it.univr.flickrclient.FlickrClientApplication;
import it.univr.flickrclient.MVC;
import it.univr.flickrclient.R;
import it.univr.flickrclient.controller.DownloadImageAsyncTask;
import it.univr.flickrclient.controller.SearchImagesAsyncTask;

import static it.univr.flickrclient.controller.Controller.BUNDLE_INFOS;
import static it.univr.flickrclient.controller.Controller.OPTION_LASTIMAGES;
import static it.univr.flickrclient.controller.Controller.OPTION_POPULARIMAGES;

    /**
    * Fragment che contiene la lista delle immagini prelevate da Flickr quando vengono richieste
    * le "Immagini Popolari" e le "Ultime Immagini". Crea il fragment contenitore della MainActivity
    */

public class ImageListFragment extends Fragment {

    private View view;
    private ListView listview;
    private String image_url;
    private MVC mvc;

    public ImageListFragment() {}


    //----------------------------------------------------------------------------------------------
    // Metodo di creazione della view del fragment
    //----------------------------------------------------------------------------------------------

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Salva la MVC condivisa nella variabile
        mvc = ((FlickrClientApplication) getActivity().getApplication()).getMVC();

        // Se viene passato come parametro l'identificativo della pagina "Ultime Immagini" allora
        // setta il titolo "Ultime Immagini" sulla'action bar. Altrimenti inserisci quello delle
        // "Immagini popolari"
        if (getArguments().getString(OPTION_LASTIMAGES) != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getActivity().getResources().getString(R.string.menubutton_lastimages));
        } else {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getActivity().getResources().getString(R.string.menubutton_popularimages));
        }

        // Se la vista è nulla allora deve essere creata, altrimenti si riusa la view creata in precedenza
        if (view == null) {
            view = inflater.inflate(R.layout.listviewimages_fragment, container, false);

            listview = (ListView) view.findViewById(R.id.list_view_results);

            // Associazione dell'evento "Click singolo"  alla listview contenente le informazioni delle immagini
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    ResultsAdapter ra = (ResultsAdapter) listview.getAdapter();

                    // Creo un hashmap per la memorizzazione delle informazioni necessarie da passare
                    // al fragment dell'immagine selezionata
                    HashMap<String, String> map = new HashMap<>();
                    map.put("id", ra.getItem(i).getId());
                    map.put("title", ra.getItem(i).getTitle());
                    map.put("url", ra.getItem(i).getUrl());

                    // Creazione del fragment dell'immagine selezionata (SelectedImage).
                    SelectedImageFragment selectedImageFragment = new SelectedImageFragment();
                    Bundle args = new Bundle();
                    args.putSerializable(BUNDLE_INFOS, map);
                    selectedImageFragment.setArguments(args);

                    // Avviamento del fragment SelectedImageFragment
                    mvc.controller.launchFragment(getActivity(), selectedImageFragment, true);

                }
            });

            // Lancio il Task asincrono per cercare le immagini. Vengono cercate le Ultime Immagini o le Immagini Popolari
            // a seconda del parametro passato al metodo launchTask.
            mvc.controller.launchTask(new SearchImagesAsyncTask(getActivity(), listview, getArguments().getString(OPTION_LASTIMAGES) != null ? OPTION_LASTIMAGES : OPTION_POPULARIMAGES));

            registerForContextMenu(listview);
        }

        return view;
    }

    //----------------------------------------------------------------------------------------------
    // Metodo che serve per creare il Menu che si apre mediante pressione prolungata
    // su un item della listview
    //----------------------------------------------------------------------------------------------

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderIcon(R.drawable.flickr);
        menu.setHeaderTitle("Select an option");
        getActivity().getMenuInflater().inflate(R.menu.menu_context, menu);
    }

    //----------------------------------------------------------------------------------------------
    // Metodo che viene lanciato quando viene cliccato (click prolungato) l'elemento item
    // della listview e setta gli elementi del menu.
    // Se si preme sull'elemento del menu si lancia il task per la condivisione dell'immagine.
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        ResultsAdapter ra = (ResultsAdapter) listview.getAdapter();
        image_url = ra.getItem(info.position).getUrl();

        // Se l'item premuto è il bottone Share allora lancio il task per la condivisione.
        // Altrimenti se l'item premuto è quello dello Stesso Autore lancia il fragment apposito.
        switch (item.getItemId()) {
            case R.id.context_menu_share:

                mvc.controller.launchTask(new DownloadImageAsyncTask(getActivity(), this, image_url));

                return true;
            case R.id.context_menu_sameauthor:

                SameAuthorImagesFragment frag = new SameAuthorImagesFragment();

                // Argomenti passati al fragment: nome dell'autore e id dell'autore
                Bundle args = new Bundle();

                args.putString("author", ra.getItem(info.position).getAuthor());
                args.putString("authorname", ra.getItem(info.position).getAuthorName());
                frag.setArguments(args);

                mvc.controller.launchFragment(getActivity(), frag, true);

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}