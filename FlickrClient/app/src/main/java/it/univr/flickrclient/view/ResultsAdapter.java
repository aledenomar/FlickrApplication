package it.univr.flickrclient.view;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import it.univr.flickrclient.R;
import it.univr.flickrclient.model.Model;

    /**
    * Classe contenente la ridefinizione dell'adapter che viene utilizzato nei risultati
    * delle ricerche delle immagini. Classe necessaria per popolare la listview delle immagini che
    * vengono popolate i comandi SEARCH, LASTIMAGES e POPULARIMAGES.
    */

public class ResultsAdapter extends ArrayAdapter<Model.Images> {

    private Context context;
    private ArrayList<Model.Images> data;
    private static LayoutInflater inflater = null;


    //----------------------------------------------------------------------------------------------
    // Costruttore dell'adapter delle immagini da mostrare
    //----------------------------------------------------------------------------------------------

    public ResultsAdapter(Context context, ArrayList<Model.Images> data) {
        super(context, R.layout.image_list_layout, data);
        this.context = context;
        this.data = data;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    //----------------------------------------------------------------------------------------------
    // Metodo che ritorna l'elemento in posizione position della lista
    //----------------------------------------------------------------------------------------------
    @Override
    public Model.Images getItem(int position) {
        return data.get(position);
    }


    //----------------------------------------------------------------------------------------------
    // Metodo di creazione della view.
    // Viene popolata la listview con la thumbnail, il titolo e la descrizione di
    // ogni immagine della lista.
    //----------------------------------------------------------------------------------------------

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View rowView = inflater.inflate(R.layout.image_list_layout, viewGroup, false);
        Spanned result;

        TextView title = (TextView) rowView.findViewById(R.id.result_object_title);

        // Il titolo (e la descrizione in seguito) se contengono link vengono passati correttamente
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(data.get(i).getTitle(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(data.get(i).getTitle());
        }

        title.setText(result);

        TextView desc = (TextView) rowView.findViewById(R.id.result_object_description);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(data.get(i).getDescription(), Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(data.get(i).getDescription());
        }

        desc.setText(result);

        // Impostazione dell'immagine d'anteprima presa tramite libreria Glide
        ImageView thumb = (ImageView) rowView.findViewById(R.id.result_object_thumb);

        Glide.with(context).load(data.get(i).getUrl()).into(thumb);

        return rowView;
    }

}