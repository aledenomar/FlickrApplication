package it.univr.flickrclient.view;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.HashMap;

import it.univr.flickrclient.FlickrClientApplication;
import it.univr.flickrclient.MVC;
import it.univr.flickrclient.R;
import it.univr.flickrclient.controller.DownloadImageAsyncTask;
import it.univr.flickrclient.controller.SearchImagesAsyncTask;

import static it.univr.flickrclient.controller.Controller.BUNDLE_INFOS;

    /**
    * Fragment che serve a cercare le immagini. E' il primo fragment che si carica quando si avvia
    * l'applicazione e contiene la search bar.
    */

public class SearchImagesFragment extends Fragment {

    private ListView listview;
    private String image_url;

    private View view;

    private MVC mvc;

    public SearchImagesFragment() {
    }

    //----------------------------------------------------------------------------------------------
    // Metodo di creazione della view del fragment
    //----------------------------------------------------------------------------------------------

    @UiThread
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //Se la lista è vuota allora la crea
        mvc = ((FlickrClientApplication) getActivity().getApplication()).getMVC();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getActivity().getResources().getString(R.string.menubutton_searchimages));

        if (view == null) {

            view = inflater.inflate(R.layout.search_images_fragment_layout, container, false);

            listview = (ListView) view.findViewById(R.id.list_view_results);
            final EditText et_search = (EditText) view.findViewById(R.id.et_search);

            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    ResultsAdapter ra = (ResultsAdapter) listview.getAdapter();

                    image_url = ra.getItem(i).getUrl();

                    HashMap<String, String> map = new HashMap<>();
                    map.put("id", ra.getItem(i).getId());
                    map.put("title", ra.getItem(i).getTitle());
                    map.put("url", image_url);

                    SelectedImageFragment frag = new SelectedImageFragment();
                    Bundle args = new Bundle();
                    args.putSerializable(BUNDLE_INFOS, map);
                    frag.setArguments(args);

                    mvc.controller.launchFragment(getActivity(), frag, true);

                }
            });

            ImageButton btn_search = (ImageButton) view.findViewById(R.id.btn_search);
            btn_search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mvc.controller.launchTask(new SearchImagesAsyncTask(getActivity(), et_search.getText().toString(), listview));

                    ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(et_search.getText().toString());
                    et_search.setText("");

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            });

            registerForContextMenu(listview);
        }

        return view;
    }

    //----------------------------------------------------------------------------------------------
    // Metodo per la creazione del menu contestuale della listview che appare con il
    // click prolungato sull'elemento della listview.
    //----------------------------------------------------------------------------------------------

    @UiThread
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderIcon(R.drawable.flickr);
        menu.setHeaderTitle("Select an option");
        getActivity().getMenuInflater().inflate(R.menu.menu_context, menu);
    }

    //----------------------------------------------------------------------------------------------
    // Metodo che viene lanciato quando viene cliccato (click prolungato) l'elemento item della
    // listview e setta gli elementi del menu.
    // Se si preme sul primo elemento del menu si lancia il task per la condivisione dell'immagine,
    // mentre se si seleziona il secondo elemento
    // del menu si lancia il fragment che contiene la listview di immagini relative allo stesso
    // autore dell'immagine selezionata in precedenza
    //----------------------------------------------------------------------------------------------

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        ResultsAdapter ra = (ResultsAdapter) listview.getAdapter();
        image_url = ra.getItem(info.position).getUrl();

        switch (item.getItemId()) {
            case R.id.context_menu_share:

                mvc.controller.launchTask(new DownloadImageAsyncTask(getActivity(), this, image_url));

                return true;
            case R.id.context_menu_sameauthor:

                SameAuthorImagesFragment frag = new SameAuthorImagesFragment();
                Bundle args = new Bundle();
                args.putString("author", ra.getItem(info.position).getAuthor());
                frag.setArguments(args);

                mvc.controller.launchFragment(getActivity(), frag, true);

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}