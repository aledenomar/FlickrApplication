package it.univr.flickrclient.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.annotation.WorkerThread;
import android.widget.ListView;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import it.univr.flickrclient.FlickrClientApplication;
import it.univr.flickrclient.MVC;
import it.univr.flickrclient.model.Model;
import it.univr.flickrclient.view.CommentsAdapter;

    //----------------------------------------------------------------------------------------------
    // Task per il recupero dei commenti delle immagini.
    //----------------------------------------------------------------------------------------------

public class DownloadImageInformationAsyncTask extends AsyncTask<Void, Void, Boolean> {

    private final Activity act;

    private JSONObject obj;
    private AlertDialog dialog;
    private String query;
    private String photoId;
    private MVC mvc;

    private ListView listView;

    // Contiene tutti i commenti dell'immagine attuale
    private ArrayList<Model.Message> messages;

    //----------------------------------------------------------------------------------------------
    // Costruttore del task
    //----------------------------------------------------------------------------------------------

    public DownloadImageInformationAsyncTask(Activity act, String photoId, ListView listView) {

        this.act = act;                 //Activity principale
        this.listView = listView;       //Listview da riempire con i commenti della foto
        this.photoId = photoId;         //ID della foto di cui prendere i commenti

        mvc = ((FlickrClientApplication) act.getApplication()).getMVC();
        messages = new ArrayList<>();

    }

    //----------------------------------------------------------------------------------------------
    // Metodo eseguito dall'AsyncTask prima del lancio del metodo doInBackground.
    // Mostra il Progress Dialog
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onPreExecute() {
        dialog = ProgressDialog.show(act, "",
                "Caricamento...", true);
        dialog.setCancelable(false);
    }


    //----------------------------------------------------------------------------------------------
    // Thread in background che effettua il download dei commenti relativi
    // all'id dell'immagine che viene passato al task
    //----------------------------------------------------------------------------------------------

    @Override
    @WorkerThread
    protected Boolean doInBackground(Void... voids) {

        // Query per la ricezione dei commenti relativi all'immagine definita da photoID
        query = String.format(
                "https://api.flickr.com/services/rest?method=flickr.photos.comments.getList&api_key=%s&photo_id=%s&format=json&nojsoncallback=1",
                mvc.controller.FLICKR_KEY,
                photoId);

        try {

            URLConnection urlConnection = new URL(query).openConnection();
            obj = new JSONObject(IOUtils.toString(urlConnection.getInputStream(), "UTF-8"));

            JSONArray arr = obj.getJSONObject("comments").getJSONArray("comment");

            // Memorizza tutti i commenti nel message
            for(int i = 0; i < arr.length(); i++){
                messages.add(new Model.Message(
                        arr.getJSONObject(i).getString(mvc.controller.FLICKR_MESSAGE_AUTHOR),
                        arr.getJSONObject(i).getString(mvc.controller.FLICKR_MESSAGE_DATE),
                        arr.getJSONObject(i).getString(mvc.controller.FLICKR_MESSAGE_TEXT)));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    //----------------------------------------------------------------------------------------------
    // Metodo lanciato successivamente al doInBackground
    //----------------------------------------------------------------------------------------------

    @Override
    protected void onPostExecute(final Boolean success) {
        listView.setAdapter(new CommentsAdapter(act, messages));
        dialog.dismiss();
    }
}